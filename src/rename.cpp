#include <stream9/linux/file/rename.hpp>

namespace stream9::linux {

void
rename(cstring_ptr oldpath, cstring_ptr newpath)
{
    auto rc = ::rename(oldpath, newpath);
    if (rc == -1) {
        throw error {
            "rename()",
            linux::make_error_code(errno), {
                { "oldpath", oldpath },
                { "newpath", newpath },
            }
        };
    }
}

} // namespace stream9::linux
