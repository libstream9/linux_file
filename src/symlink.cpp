#include <stream9/linux/symlink.hpp>

namespace stream9::linux {

void
symlink(cstring_ptr target, cstring_ptr linkpath)
{
    auto rc = ::symlink(target, linkpath);
    if (rc == -1) {
        throw error {
            "symlink(2)",
            lx::make_error_code(errno), {
                { "target", target },
                { "linkpath", linkpath },
            }
        };
    }
}

} // namespace stream9::linux
