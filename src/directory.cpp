#include <stream9/linux/file/directory.hpp>

#include <stream9/linux/fd.hpp>
#include <stream9/linux/file/open.hpp>

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

#include <stream9/linux/error.hpp>
#include <stream9/log.hpp>

namespace stream9::linux {

directory::
directory(cstring_ptr const& pathname)
    : m_dir { ::opendir(pathname) }
{
    try {
        if (m_dir == nullptr) {
            throw error {
                "opendir()",
                linux::make_error_code(errno)
            };
        }
    }
    catch (...) {
        rethrow_error({
            { "pathname", pathname }
        });
    }
}

directory::
directory(linux::fd&& fd)
    : m_dir { ::fdopendir(fd) }
{
    try {
        if (m_dir == nullptr) {
            throw error {
                "fdopendir()",
                linux::make_error_code(errno)
            };
        }

        fd.release();
    }
    catch (...) {
        rethrow_error({
            { "fd", fd },
            { "path", fd.path() }
        });
    }
}

directory::
directory(fd_ref dirfd, cstring_ptr const& name)
{
    try {
        auto fd = linux::openat(dirfd, name, O_RDONLY | O_CLOEXEC);

        m_dir = ::fdopendir(fd);
        if (m_dir == nullptr) {
            throw error {
                "fdopendir()",
                linux::make_error_code(errno)
            };
        }

        fd.release();
    }
    catch (...) {
        rethrow_error({
            { "dirfd", dirfd },
            { "name", name }
        });
    }
}

directory::
~directory() noexcept
{
    try {
        if (m_dir) {
            auto const rc = ::closedir(m_dir);
            if (rc == -1) {
                throw error {
                    "closedir()",
                    linux::make_error_code(errno),
                };
            }
        }
    }
    catch (...) {
        stream9::errors::print_error(log::err());
    }
}

directory::
directory(directory&& o) noexcept
    : m_dir { o.m_dir }
{
    o.m_dir = nullptr;
}

directory& directory::
operator=(directory&& o) noexcept
{
    directory::~directory();
    m_dir = o.m_dir;
    o.m_dir = nullptr;
    return *this;
}

fd_ref directory::
fd() const
{
    try {
        auto const rc = ::dirfd(m_dir);
        if (rc == -1) {
            throw error {
                "dirfd()",
                linux::make_error_code(errno),
            };
        }

        return rc;
    }
    catch (...) {
        rethrow_error();
    }
}

opt<struct ::dirent const&> directory::
read() const
{
    try {
        errno = 0;
        auto* const ent = ::readdir(m_dir);
        if (ent == nullptr) {
            if (errno) {
                throw error {
                    "readdir()",
                    linux::make_error_code(errno),
                };
            }
            else {
                return {};
            }
        }

        return *ent;
    }
    catch (...) {
        rethrow_error();
    }
}

long directory::
tell() const
{
    try {
        auto const rc = ::telldir(m_dir);
        if (rc == -1) {
            throw error {
                "telldir()",
                linux::make_error_code(errno),
            };
        }

        return rc;
    }
    catch (...) {
        rethrow_error();
    }
}

static unsigned char
to_type(mode_t m)
{
    if (S_ISREG(m)) {
        return DT_REG;
    }
    else if (S_ISDIR(m)) {
        return DT_DIR;
    }
    else if (S_ISCHR(m)) {
        return DT_CHR;
    }
    else if (S_ISBLK(m)) {
        return DT_BLK;
    }
    else if (S_ISFIFO(m)) {
        return DT_FIFO;
    }
    else if (S_ISLNK(m)) {
        return DT_LNK;
    }
    else if (S_ISSOCK(m)) {
        return DT_SOCK;
    }
    else {
        return DT_UNKNOWN;
    }
}

unsigned char
type(struct ::dirent const& e, fd_ref dirfd)
{
    try {
        if (e.d_type != DT_LNK && e.d_type != DT_UNKNOWN) {
            return e.d_type;
        }
        else {
            struct ::stat buf {};
            auto rc = ::fstatat(dirfd, e.d_name, &buf, 0);
            if (rc == -1) {
                throw error {
                    "fstatat()",
                    lx::make_error_code(errno),
                };
            }

            return to_type(buf.st_mode);
        }
    }
    catch (...) {
        rethrow_error({
            { "d_name", e.d_name },
            { "d_type", dirent_type_to_symbol(e.d_type) },
        });
    }
}

unsigned char
ltype(struct ::dirent const& e, fd_ref dirfd)
{
    try {
        if (e.d_type != DT_UNKNOWN) {
            return e.d_type;
        }
        else {
            struct ::stat buf {};
            auto rc = ::fstatat(dirfd, e.d_name, &buf, AT_SYMLINK_NOFOLLOW);
            if (rc == -1) {
                throw error {
                    "fstatat()",
                    lx::make_error_code(errno),
                };
            }

            return to_type(buf.st_mode);
        }
    }
    catch (...) {
        rethrow_error({
            { "d_name", e.d_name },
            { "d_type", dirent_type_to_symbol(e.d_type) },
        });
    }
}

} // namespace stream9::linux
