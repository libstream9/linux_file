#ifndef STREAM9_LINUX_FILE_NAMESPACE_HPP
#define STREAM9_LINUX_FILE_NAMESPACE_HPP

namespace stream9::errors {}
namespace stream9::json {}
namespace stream9::strings {}

namespace stream9::linux {

namespace err { using namespace stream9::errors; }
namespace json { using namespace stream9::json; }
namespace str { using namespace stream9::strings; }

} // namespace stream9::linux

#endif // STREAM9_LINUX_FILE_NAMESPACE_HPP
