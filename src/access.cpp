#include <stream9/linux/file/access.hpp>

#include <stream9/linux/error.hpp>

#include <stream9/strings/stream.hpp>

namespace stream9::linux {

static std::string
mode_to_string(int const mode)
{
    std::string s;

    if (mode == F_OK) {
        s = "F_OK";
    }
    else {
        if ((mode & R_OK) == R_OK) {
            s.append("R_OK");
        }
        if ((mode & W_OK) == W_OK) {
            if (!s.empty()) s.append(" | ");

            s.append("W_OK");
        }
        if ((mode & X_OK) == X_OK) {
            if (!s.empty()) s.append(" | ");

            s.append("X_OK");
        }
        if (s.empty()) {
            using str::operator<<;
            s << "unknown value: " << mode;
        }
    }

    return s;
}

bool
access(cstring_ptr pathname, int mode)
{
    auto rc = ::access(pathname, mode);
    if (rc == -1) {
        throw error {
            "access()",
            linux::make_error_code(errno), {
                { "pathname", pathname },
                { "mode", mode_to_string(mode) },
            }
        };
    }

    return true;
}

bool
euidaccess(cstring_ptr pathname, int mode)
{
    auto rc = ::euidaccess(pathname, mode);
    if (rc == -1) {
        throw error {
            "euidaccess()",
            linux::make_error_code(errno), {
                { "pathname", pathname },
                { "mode", mode_to_string(mode) },
            }
        };
    }

    return true;
}

} // namespace stream9::linux
