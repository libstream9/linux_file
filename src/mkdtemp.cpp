#include <stream9/linux/mkdtemp.hpp>

#include <stdlib.h>

namespace stream9::linux {

string
mkdtemp()
{
    string p { "XXXXXX" };

    if (::mkdtemp(p.data()) == nullptr) {
        throw_error(lx::make_error_code(errno));
    }

    return p;
}

string
mkdtemp(string_view prefix)
{
    string p { prefix };
    p += "XXXXXX";

    if (::mkdtemp(p.data()) == nullptr) {
        throw_error(lx::make_error_code(errno));
    }

    return p;
}

} // namespace stream9::linux
