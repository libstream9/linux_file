#include <stream9/linux/file/mmap.hpp>

#include <stream9/linux/file/open.hpp>
#include <stream9/linux/file/stat.hpp>
#include <stream9/linux/file/symbol.hpp>

#include <stream9/json.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/to_string.hpp>

namespace stream9::linux {

static std::pair<void*, std::size_t>
do_mmap(void* const addr,
     std::size_t const length,
     int const prot,
     int const flags,
     fd_ref const fd,
     ::off_t const offset)
{
    auto const ptr = ::mmap(addr, length, prot, flags, fd, offset);
    if (ptr == MAP_FAILED) {
        throw error { "mmap()",
            linux::make_error_code(errno), {
                { "addr", str::to_string(addr), },
                { "length", length, },
                { "prot", mmap_prot_to_symbol(prot), },
                { "flags", mmap_flags_to_symbol(flags), },
                { "fd", fd.path(), },
                { "offset", offset, },
            }
        };
    }

    return { ptr, length };
}

static std::pair<void*, std::size_t>
do_mmap(fd_ref const fd, int const access_mode)
{
    try {
        int prot;
        switch (access_mode) {
            case O_RDONLY:
                prot = PROT_READ;
                break;
            case O_WRONLY:
                prot = PROT_WRITE;
                break;
            case O_RDWR:
                prot = PROT_READ | PROT_WRITE;
                break;
            default:
                assert(false); // invalid value must be caught at preceeding open()
                break;
        }

        auto const& st = fstat(fd);
        if (st.st_size == 0) {
            throw error { mmap::errc::try_to_map_empty_file };
        }

        return do_mmap(nullptr,
            static_cast<std::size_t>(st.st_size), prot, MAP_PRIVATE, fd, 0);
    }
    catch (...) {
        st9::rethrow_error(mmap::error_category(), {
            { "fd", fd },
        });
    }
}

mmap::
mmap(void* const addr,
     std::size_t const length,
     int const prot,
     int const flags,
     fd_ref const fd,
     ::off_t const offset)
{
    std::tie(m_addr, m_len) = do_mmap(addr, length, prot, flags, fd, offset);
}

mmap::
mmap(cstring_ptr const& pathname)
{
    try {
        auto const fd = open(pathname, O_RDONLY);

        std::tie(m_addr, m_len) = do_mmap(fd, O_RDONLY);
    }
    catch (...) {
        st9::rethrow_error(error_category(), {
            { "pathname", pathname, },
        });
    }
}

mmap::
mmap(fd_ref const fd)
{
    try {
        std::tie(m_addr, m_len) = do_mmap(fd, fd.access_mode());
    }
    catch (...) {
        rethrow_error({ { "fd", fd }, });
    }
}

mmap::
~mmap() noexcept
{
    if (m_addr) {
        if (::munmap(m_addr, m_len) == -1) {
            //TODO log errno
        }
    }
}

std::error_category const& mmap::
error_category()
{
    static struct impl : std::error_category {
        char const* name() const noexcept override
        {
            return "stream9::linux::mmap";
        }

        std::string message(int const e) const override
        {
            using enum errc;

            switch (static_cast<errc>(e)) {
                case try_to_map_empty_file:
                    return "try to map empty file";
            }

            return "unknown error";
        }
    } instance;

    return instance;
}

} // namespace stream9::linux
