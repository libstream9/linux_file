#include <stream9/linux/fnmatch.hpp>

#include <stream9/linux/error.hpp>

#include <fnmatch.h>

namespace stream9::linux {

bool
fnmatch(cstring_ptr const& pattern,
        cstring_ptr const& pathname,
        int flags)
{
    auto rv = ::fnmatch(pattern, pathname, flags);

    switch (rv) {
        case 0:
            return true;
        case FNM_NOMATCH:
            return false;
        // According to the document, GNU libc implementation doesn't cause error.
        // But, that can change in the future, so...
        default:
            throw_error(
                "fnmatch()",
                stream9::errc::internal_error, {
                    { "pattern", pattern },
                    { "pathname", pathname },
                    { "flags", flags },
                    { "rv", rv },
                }
            );
    }
}


} // namespace stream9::linux
