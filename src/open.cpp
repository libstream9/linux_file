#include <stream9/linux/file/open.hpp>

#include <stream9/linux/file/symbol.hpp>

#include <fcntl.h>

#include <stream9/json.hpp>
#include <stream9/linux/error.hpp>

namespace stream9::linux {

fd
open(cstring_ptr const& pathname, int flags)
{
    auto rv = ::open(pathname, flags, 0666);
    if (rv == -1) {
        throw error { "open()",
            linux::make_error_code(errno), {
                { "pathname", pathname },
                { "flags", file_status_flags_to_symbol(flags) },
            }
        };
    }

    return fd { rv };
}

fd
open(cstring_ptr const& pathname, int flags, ::mode_t mode)
{
    auto rv = ::open(pathname, flags, mode);
    if (rv == -1) {
        throw error { "open()",
            linux::make_error_code(errno), {
                { "pathname", pathname },
                { "flags", file_status_flags_to_symbol(flags) },
                { "mode", file_mode_to_symbol(mode) },
            }
        };
    }

    return fd { rv };
}

fd
openat(fd_ref dirfd, cstring_ptr const& pathname, int flags)
{
    auto rv = ::openat(dirfd, pathname, flags, 0666);
    if (rv == -1) {
        throw error { "openat()",
            linux::make_error_code(errno), {
                { "dirfd", dirfd },
                { "pathname", pathname },
                { "flags", file_status_flags_to_symbol(flags) },
            }
        };
    }

    return fd { rv };
}

fd
openat(fd_ref dirfd,
       cstring_ptr const& pathname,
       int flags,
       ::mode_t mode)
{
    auto rv = ::openat(dirfd, pathname, flags, mode);
    if (rv == -1) {
        throw error { "openat()",
            linux::make_error_code(errno), {
                { "dirfd", dirfd },
                { "pathname", pathname },
                { "flags", file_status_flags_to_symbol(flags) },
                { "mode", file_mode_to_symbol(mode) },
            }
        };
    }

    return fd { rv };
}

} // namespace stream9::linux

namespace stream9::linux::nothrow {

outcome<fd, linux::errc>
open(cstring_ptr const& pathname, int flags)
{
    auto rv = ::open(pathname, flags, 0666);
    if (rv == -1) {
        return { st9::error_tag(), static_cast<linux::errc>(errno) };
    }
    else {
        return rv;
    }
}

outcome<fd, linux::errc>
open(cstring_ptr const& pathname, int flags, ::mode_t mode)
{
    auto rv = ::open(pathname, flags, mode);
    if (rv == -1) {
        return { st9::error_tag(), static_cast<linux::errc>(errno) };
    }
    else {
        return rv;
    }
}

outcome<fd, linux::errc>
openat(fd_ref dirfd, cstring_ptr const& pathname, int flags)
{
    auto rv = ::openat(dirfd, pathname, flags, 0666);
    if (rv == -1) {
        return { st9::error_tag(), static_cast<linux::errc>(errno) };
    }
    else {
        return rv;
    }
}

outcome<fd, linux::errc>
openat(fd_ref dirfd,
       cstring_ptr const& pathname,
       int flags,
       ::mode_t mode)
{
    auto rv = ::openat(dirfd, pathname, flags, mode);
    if (rv == -1) {
        return { st9::error_tag(), static_cast<linux::errc>(errno) };
    }
    else {
        return rv;
    }
}

} // namespace stream9::linux::nothrow
