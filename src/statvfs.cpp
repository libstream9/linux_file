#include <stream9/linux/statvfs.hpp>

#include <stream9/bits.hpp>

namespace stream9::linux {

static std::string
mount_flags_to_symbol(unsigned long flags)
{
    return bits::ored_flags_to_symbol(static_cast<int>(flags), {
        STREAM9_FLAG_ENTRY(ST_MANDLOCK),
        STREAM9_FLAG_ENTRY(ST_NOATIME),
        STREAM9_FLAG_ENTRY(ST_NODEV),
        STREAM9_FLAG_ENTRY(ST_NODIRATIME),
        STREAM9_FLAG_ENTRY(ST_NOEXEC),
        STREAM9_FLAG_ENTRY(ST_NOSUID),
        STREAM9_FLAG_ENTRY(ST_RDONLY),
        STREAM9_FLAG_ENTRY(ST_RELATIME),
        STREAM9_FLAG_ENTRY(ST_SYNCHRONOUS),
        //STREAM9_FLAG_ENTRY(ST_NOSYMFOLLOW), // not in glibc 2.36
    });
}

struct ::statvfs
statvfs(cstring_ptr const& path)
{
    struct ::statvfs buf {};
    auto rc = ::statvfs(path, &buf);
    if (rc == -1) {
        throw error {
            "statvfs()",
            linux::make_error_code(errno), {
                { "path", path }
            }
        };
    }

    return buf;
}

struct ::statvfs
fstatvfs(fd_ref fd)
{
    struct ::statvfs buf {};
    auto rc = ::fstatvfs(fd, &buf);
    if (rc == -1) {
        throw error {
            "fstatvfs()",
            linux::make_error_code(errno), {
                { "fd", fd }
            }
        };
    }

    return buf;
}

} // namespace stream9::linux

namespace stream9::json {

void
tag_invoke(json::value_from_tag, json::value& jv, struct ::statvfs const& st)
{
    auto& obj = jv.emplace_object();

    obj["f_bsize"] = st.f_bsize;
    obj["f_frsize"] = st.f_frsize;
    obj["f_blocks"] = st.f_blocks;
    obj["f_bfree"] = st.f_bfree;
    obj["f_bavail"] = st.f_bavail;

    obj["f_files"] = st.f_files;
    obj["f_ffree"] = st.f_ffree;
    obj["f_favail"] = st.f_favail;

    obj["f_fsid"] = st.f_fsid;
    obj["f_flag"] = stream9::linux::mount_flags_to_symbol(st.f_flag);
    obj["f_namemax"] = st.f_namemax;
}

} // namespace stream9::json
