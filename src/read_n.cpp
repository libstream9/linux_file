#include <stream9/linux/read_n.hpp>

#include <cstdint>

namespace stream9::linux {

using std::size_t;

read_n_outcome
read_n(fd_ref fd,
       array_view<char> buf) noexcept
{
    auto first = buf.begin();
    auto last = buf.end();
    assert(first <= last);
    bool is_eof = false;

    while (first < last) {
        auto n = ::read(fd, first, static_cast<size_t>(last - first));
        if (n == -1) {
            if (errno == EINTR) continue;
            else {
                return {
                    first - buf.begin(),
                    static_cast<errc>(errno),
                    is_eof
                };
            }
        }
        else if (n == 0) { // EOF
            is_eof = true;
            break;
        }
        else {
            first += n;
        }
    }

    return {
        first - buf.begin(),
        errc(),
        is_eof
    };
}

} // namespace stream9::linux
