#include <stream9/linux/chmod.hpp>

#include <stream9/linux/file/symbol.hpp>

#include <sys/stat.h>

namespace stream9::linux {

void
chmod(cstring_ptr pathname, ::mode_t mode)
{
    auto rc = ::chmod(pathname, mode);
    if (rc == -1) {
        throw error {
            "chmod(2)",
            lx::make_error_code(errno), {
                { "pathname", pathname },
                { "mode", file_mode_to_symbol(mode) }
            }
        };
    }
}

} // namespace stream9::linux
