#include <stream9/linux/getcwd.hpp>

#include <stdlib.h>

namespace stream9::linux {

string
getcwd()
{
    try {
        struct free_fn {
            void operator()(char* p) const noexcept
            {
                ::free(p);
            }
        };

        std::unique_ptr<char, free_fn> rv { ::getcwd(nullptr, 0) };
        if (rv == nullptr) {
            throw error {
                linux::make_error_code(errno)
            };
        }
        else {
            return rv.get();
        }
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::linux
