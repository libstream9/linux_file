#include <stream9/linux/remove.hpp>

#include <stdio.h>

#include <stream9/linux/error.hpp>

namespace stream9::linux {

void
remove(cstring_ptr const& p)
{
    auto rc = ::remove(p);
    if (rc == -1) {
        throw error {
            "remove(3)",
            lx::make_error_code(errno), {
                { "pathname", p }
            }
        };
    }
}

} // namespace stream9::linux
