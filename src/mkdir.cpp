#include <stream9/linux/file/mkdir.hpp>

#include <stream9/linux/file/symbol.hpp>

namespace stream9::linux {

void
mkdir(cstring_ptr const& pathname, ::mode_t mode/*= 0666*/)
{
    auto rc = ::mkdir(pathname, mode);
    if (rc == -1) {
        throw error {
            "mkdir()",
            linux::make_error_code(errno), {
                { "pathname", pathname },
                { "mode", file_mode_to_symbol(mode) },
            }
        };
    }
}

namespace nothrow {

outcome<void, errc>
mkdir(cstring_ptr const& pathname, ::mode_t mode/*= 0666*/)
{
    auto rc = ::mkdir(pathname, mode);
    if (rc == -1) {
        return static_cast<errc>(errno);
    }
    else {
        return errc();
    }
}

} // namespace nothrow

} // namespace stream9::linux
