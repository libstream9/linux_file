#include <stream9/linux/link.hpp>

#include <stdio.h>

namespace stream9::linux {

void
linkat(fd_ref olddirfd, cstring_ptr const& oldpath,
       fd_ref newdirfd, cstring_ptr const& newpath, int flags/*= 0*/)
{

    auto rc = ::linkat(olddirfd, oldpath, newdirfd, newpath, flags);
    if (rc == -1) {
        throw error {
            "linkat(2)", lx::make_error_code(errno), {
                { "olddirfd", olddirfd },
                { "oldpath", oldpath },
                { "newdirfd", newdirfd },
                { "newpath", newpath },
                { "flags", flags } //TODO
            }
        };
    }
}

void
linkat(fd_ref file,
       fd_ref newdirfd, cstring_ptr const& newpath)
{
    try {
        char oldpath[PATH_MAX];
        auto rc = ::snprintf(
            oldpath, PATH_MAX, "/proc/self/fd/%d", static_cast<int>(file));
        if (rc < 0 && errno != 0) {
            throw error {
                "snprintf(3)", lx::make_error_code(errno)
            };
        }

        linkat(file, oldpath, newdirfd, newpath, AT_SYMLINK_FOLLOW);

        // If process have CAP_DAC_READ_SEARCH, then following works too.
        //linkat(file, "", newdirfd, newpath, AT_EMPTY_PATH);
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::linux
