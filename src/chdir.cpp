#include <stream9/linux/chdir.hpp>

#include <unistd.h>

namespace stream9::linux {

void
chdir(cstring_ptr const& path)
{
    auto rc = ::chdir(path);
    if (rc == -1) {
        throw error {
            "chdir(2)",
            lx::make_error_code(errno), {
                { "path", path }
            }
        };
    }
}

void
chdir(fd_ref fd)
{
    auto rc = ::fchdir(fd);
    if (rc == -1) {
        throw error {
            "fchdir(2)",
            lx::make_error_code(errno), {
                { "fd", fd }
            }
        };
    }
}

} // namespace stream9::linux
