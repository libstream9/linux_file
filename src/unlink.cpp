#include <stream9/linux/file/unlink.hpp>

#include <unistd.h>

namespace stream9::linux {

void
unlink(cstring_ptr const& pathname)
{
    auto rc = ::unlink(pathname);
    if (rc == -1) {
        throw error {
            "unlink(2)",
            linux::make_error_code(errno), {
                { "pathname", pathname },
            }
        };
    }
}

void
unlink(fd_ref dir, cstring_ptr const& pathname)
{
    auto rc = ::unlinkat(dir, pathname, 0);
    if (rc == -1) {
        throw error {
            "unlink(2)",
            linux::make_error_code(errno), {
                { "dir", dir },
                { "pathname", pathname },
            }
        };
    }
}

namespace nothrow {

outcome<void, errc>
unlink(cstring_ptr const& pathname)
{
    auto rc = ::unlink(pathname);
    if (rc == -1) {
        return static_cast<lx::errc>(errno);
    }
    else {
        return lx::errc();
    }
}

outcome<void, errc>
unlink(fd_ref dir, cstring_ptr const& pathname)
{
    auto rc = ::unlinkat(dir, pathname, 0);
    if (rc == -1) {
        return static_cast<lx::errc>(errno);
    }
    else {
        return lx::errc();
    }
}

} // namespace nothrow

} // namespace stream9::linux
