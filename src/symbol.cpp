#include <stream9/linux/file/symbol.hpp>

#include "namespace.hpp"

#include <stream9/array.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/push_back.hpp>
#include <stream9/strings/join.hpp>
#include <stream9/strings/stream.hpp>

#include <span>

#include <sys/mman.h>

#include <dirent.h>

namespace stream9::linux {

template<typename T>
struct symbol_entry
{
    T value;
    char const* symbol;
};

template<typename T>
static std::string
bit_set_to_symbol_set(std::span<symbol_entry<T>> const table, T value)
{
    try {
        array<char const*, 5> flags;

        for (auto const& ent: table) {
            if (ent.value & value) {
                value &= ~ent.value;
                st9::push_back(flags, ent.symbol);
            }
        }

        auto result = str::join(flags, " | ");

        if (value != 0) {
            using str::operator<<;

            if (!result.empty()) {
                result << " | ";
            }
            result << "unknown(0" << std::oct << value << ")";
        }

        return result;
    }
    catch (...) {
        lx::rethrow_error();
    }
}

template<typename T>
char const*
value_to_symbol(std::span<symbol_entry<T>> const table, T const value)
{
    for (auto const& ent: table) {
        if (ent.value == value) {
            return ent.symbol;
        }
    }

    return nullptr;
}

template<typename T>
T
bit_set_to_symbol_set(auto& flags,
                      std::span<symbol_entry<T>> const table,
                      T value)
{
    try {
        for (auto const& ent: table) {
            if (ent.value & value) {
                value &= ~ent.value;
                st9::push_back(flags, ent.symbol);
            }
        }

        return value;
    }
    catch (...) {
        rethrow_error();
    }
}

static std::string
join_flags(auto const& flags,
           std::integral auto const remained,
           int const base)
{
    try {
        auto result = str::join(flags, " | ");

        if (remained != 0) {
            using str::operator<<;

            if (!result.empty()) {
                result << " | ";
            }

            if (base == 8) {
                result << "0";
            }
            else if (base == 16) {
                result << "0x";
            }
            result << std::setbase(base) << remained;
        }

        return result;
    }
    catch (...) {
        rethrow_error({
            { "remained", remained },
            { "base", base },
        });
    }
}

std::string
file_descriptor_flags_to_symbol(int const flags)
{
    try {
        std::string result;

        if (flags == FD_CLOEXEC) {
            result = "FD_CLOEXEC";
        }
        else if (flags) {
            using str::operator<<;

            result << "0x" << std::hex << flags << "";
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

static int
file_access_mode_to_symbol(auto& flags, int const v)
{
    try {
        switch (v & O_ACCMODE) {
            case O_RDONLY:
                st9::push_back(flags, "O_RDONLY");
                break;
            case O_WRONLY:
                st9::push_back(flags, "O_WRONLY");
                break;
            case O_RDWR:
                st9::push_back(flags, "O_RDWR");
                break;
            default:
                break;
        }

        return v & ~O_ACCMODE;
    }
    catch (...) {
        rethrow_error();
    }
}

static int
file_creation_flags_to_symbols(auto& flags, int const v)
{
    try {
        static symbol_entry<int> tbl[] = {
            { O_CLOEXEC, "O_CLOEXEC" },
            { O_CREAT, "O_CREAT" },
            { O_DIRECTORY, "O_DIRECTORY" },
            { O_EXCL, "O_EXCL" },
            { O_NOCTTY, "O_NOCTTY" },
            { O_NOFOLLOW, "O_NOFOLLOW" },
            { O_TMPFILE, "O_TMPFILE" },
            { O_TRUNC, "O_TRUNC" },
        };

        return bit_set_to_symbol_set<int>(flags, tbl, v);
    }
    catch (...) {
        rethrow_error();
    }
}

static int
file_status_flags_to_symbols(auto& flags, int const v)
{
    try {
        static symbol_entry<int> tbl[] = {
            { O_APPEND, "O_APPEND" },
            { O_ASYNC, "O_ASYNC" },
            { O_DIRECT, "O_DIRECT" },
            { O_DSYNC, "O_DSYNC" },
            { O_LARGEFILE, "O_LARGEFILE" },
            { O_NOATIME, "O_NOATIME" },
            { O_NONBLOCK, "O_NONBLOCK" },
            { O_PATH, "O_PATH" },
            { O_SYNC, "O_SYNC" },
        };

        return bit_set_to_symbol_set<int>(flags, tbl, v);
    }
    catch (...) {
        rethrow_error();
    }
}

std::string
file_status_flags_to_symbol(int v)
{
    try {
        array<char const*, 10> flags;

        v = file_access_mode_to_symbol(flags, v);
        v = file_creation_flags_to_symbols(flags, v);
        v = file_status_flags_to_symbols(flags, v);

        auto result = str::join(flags, " | ");

        if (v != 0) {
            using str::operator<<;

            if (!result.empty()) {
                result << " | ";
            }
            result << "unknown(0" << std::oct << v << ")";
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

std::string
file_mode_to_symbol(::mode_t const flags)
{
    try {
        static symbol_entry<::mode_t> tbl[] = {
            { S_IRUSR, "S_IRUSR" },
            { S_IWUSR, "S_IWUSR" },
            { S_IXUSR, "S_IXUSR" },
            { S_IRGRP, "S_IRGRP" },
            { S_IWGRP, "S_IWGRP" },
            { S_IXGRP, "S_IXGRP" },
            { S_IROTH, "S_IROTH" },
            { S_IWOTH, "S_IWOTH" },
            { S_IXOTH, "S_IXOTH" },
            { S_ISUID, "S_ISUID" },
            { S_ISGID, "S_ISGID" },
            { S_ISVTX, "S_ISVTX" },
        };

        return bit_set_to_symbol_set<::mode_t>(tbl, flags);
    }
    catch (...) {
        rethrow_error();
    }
}

std::string
file_type_and_mode_to_symbol(::mode_t const tm)
{
    try {
        auto const m = tm & 07777;
        auto const t = tm & S_IFMT;

        auto s = file_mode_to_symbol(m);

        if (!s.empty() && t) {
            s.insert(0, " | ");
        }

        if (t == S_IFSOCK) {
            s.insert(0, "S_IFSOCK");
        }
        else if (t == S_IFLNK) {
            s.insert(0, "S_IFLNK");
        }
        else if (t == S_IFREG) {
            s.insert(0, "S_IFREG");
        }
        else if (t == S_IFBLK) {
            s.insert(0, "S_IFBLK");
        }
        else if (t == S_IFDIR) {
            s.insert(0, "S_IFDIR");
        }
        else if (t == S_IFCHR) {
            s.insert(0, "S_IFCHR");
        }
        else if (t == S_IFIFO) {
            s.insert(0, "S_IFIFO");
        }

        return s;
    }
    catch (...) {
        rethrow_error({ { "value", tm }, });
    }
}

std::string
fstatat_flags_to_symbol(int f)
{
    try {
        static symbol_entry<int> tbl[] = {
            { AT_EMPTY_PATH, "AT_EMPTY_PATH", },
            { AT_NO_AUTOMOUNT, "AT_NO_AUTOMOUNT", },
            { AT_SYMLINK_NOFOLLOW, "AT_SYMLINK_NOFOLLOW", },
        };

        array<char const*, 3> flags;

        f = bit_set_to_symbol_set<int>(flags, tbl, f);

        return join_flags(flags, f, 16);
    }
    catch (...) {
        rethrow_error({ { "flags", f }, });
    }
}

std::string
mmap_prot_to_symbol(int prot)
{
    try {
        if (prot == PROT_NONE) {
            return "PROT_NONE";
        }
        else {
            static symbol_entry<int> tbl[] = {
                { PROT_READ, "PROT_READ", },
                { PROT_WRITE, "PROT_WRITE", },
                { PROT_EXEC, "PROT_EXEC", },
            };

            array<char const*, 3> flags;

            prot = bit_set_to_symbol_set<int>(flags, tbl, prot);

            return join_flags(flags, prot, 16);
        }
    }
    catch (...) {
        rethrow_error({ { "prot", prot }, });
    }
}

std::string
mmap_flags_to_symbol(int value)
{
    try {
        array<char const*, 5> flags;

        static symbol_entry<int> type_tbl[] = {
            { MAP_SHARED, "MAP_SHARED", },
            { MAP_SHARED_VALIDATE, "MAP_SHARED_VALIDATE", },
            { MAP_PRIVATE, "MAP_PRIVATE", },
        };

        auto const* type = value_to_symbol<int>(type_tbl, value & MAP_TYPE);
        if (type) {
            st9::push_back(flags, type);
        }

        static symbol_entry<int> flag_tbl[] = {
            { MAP_32BIT, "MAP_32BIT", },
            { MAP_ANONYMOUS, "MAP_ANONYMOUS", },
            { MAP_DENYWRITE, "MAP_DENYWRITE", },
            { MAP_EXECUTABLE, "MAP_EXECUTABLE", },
            { MAP_FILE, "MAP_FILE", },
            { MAP_FIXED, "MAP_FIXED", },
            { MAP_FIXED_NOREPLACE, "MAP_FIXED_NOREPLACE", },
            { MAP_GROWSDOWN, "MAP_GROWSDOWN", },
            { MAP_HUGETLB, "MAP_HUGETLB", },
            //{ MAP_HUGE_2MB, "MAP_HUGE_2MB", }, // glibc doesn't have them
            //{ MAP_HUGE_1GB, "MAP_HUGE_1GB", },
            { MAP_LOCKED, "MAP_LOCKED", },
            { MAP_NONBLOCK, "MAP_NONBLOCK", },
            { MAP_NORESERVE, "MAP_NORESERVE", },
            { MAP_POPULATE, "MAP_POPULATE", },
            { MAP_STACK, "MAP_STACK", },
            { MAP_SYNC, "MAP_SYNC", },
            //{ MAP_UNINITIALIZED, "MAP_UNINITIALIZED", }, // glibc doesn't have this one too
        };

        value = bit_set_to_symbol_set<int>(flags, flag_tbl, value & ~MAP_TYPE);

        return join_flags(flags, value, 16);
    }
    catch (...) {
        rethrow_error({ { "flags", value }, });
    }
}

std::string
dirent_type_to_symbol(unsigned char const d_type)
{
    switch (d_type) {
        case DT_BLK: return "DT_BLK";
        case DT_CHR: return "DT_CHR";
        case DT_DIR: return "DT_DIR";
        case DT_FIFO: return "DT_FIFO";
        case DT_LNK: return "DT_LNK";
        case DT_REG: return "DT_REG";
        case DT_SOCK: return "DT_SOCK";
        case DT_UNKNOWN: return "DT_UNKNOWN";
    }

    using str::operator<<;
    std::string result;

    result << "unknown(" << d_type << ")";

    return result;
}

} // namespace stream9::linux
