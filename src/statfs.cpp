#include <stream9/linux/file/statfs.hpp>

#include <stream9/bits.hpp>
#include <stream9/to_string.hpp>

namespace stream9::linux {

static std::string
flags_to_symbol(__fsword_t flags)
{
    return bits::ored_flags_to_symbol(static_cast<int>(flags), {
        STREAM9_FLAG_ENTRY(ST_MANDLOCK),
        STREAM9_FLAG_ENTRY(ST_NOATIME),
        STREAM9_FLAG_ENTRY(ST_NODEV),
        STREAM9_FLAG_ENTRY(ST_NODIRATIME),
        STREAM9_FLAG_ENTRY(ST_NOEXEC),
        STREAM9_FLAG_ENTRY(ST_NOSUID),
        STREAM9_FLAG_ENTRY(ST_RDONLY),
        STREAM9_FLAG_ENTRY(ST_RELATIME),
        STREAM9_FLAG_ENTRY(ST_SYNCHRONOUS),
        //STREAM9_FLAG_ENTRY(ST_NOSYMFOLLOW), // not in glibc 2.36
    });
}

struct ::statfs
statfs(cstring_ptr const& path)
{
    struct ::statfs buf {};
    auto rc = ::statfs(path, &buf);
    if (rc == -1) {
        throw error {
            "statfs()",
            linux::make_error_code(errno), {
                { "path", path }
            }
        };
    }

    return buf;
}

} // namespace stream9::linux

namespace stream9::json {

void
tag_invoke(json::value_from_tag, json::value& jv, struct ::statfs const& st)
{
    using stream9::strings::to_string;

    auto& obj = jv.emplace_object();

    obj["f_type"] = to_string(st.f_type, 16);
    obj["f_bsize"] = st.f_bsize;
    obj["f_blocks"] = st.f_blocks;
    obj["f_bfree"] = st.f_bfree;
    obj["f_bavail"] = st.f_bavail;
    obj["f_files"] = st.f_files;
    obj["f_ffree"] = st.f_ffree;
    obj["f_fsid"] = json::array {
        to_string(st.f_fsid.__val[0], 16),
        to_string(st.f_fsid.__val[1], 16)
    };
    obj["f_namelen"] = st.f_namelen;
    obj["f_frsize"] = st.f_frsize;
    obj["f_flags"] = stream9::linux::flags_to_symbol(st.f_flags);
}

} // namespace stream9::json
