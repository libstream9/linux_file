#include <stream9/linux/file/readlink.hpp>

#include <stream9/linux/file/stat.hpp>

#include <stream9/linux/error.hpp>

namespace stream9::linux {

std::string
readlink(cstring_ptr const& pathname)
{
    try {
        std::string result;

        auto const sb = lstat(pathname);
        if (sb.st_size == 0) {
            result.resize(PATH_MAX);
        }
        else {
            result.resize(static_cast<std::size_t>(sb.st_size) + 1);
        }

        readlink(pathname, result);

        return result;
    }
    catch (...) {
        rethrow_error({ { "pathname", pathname } });
    }
}

void
readlink(cstring_ptr const& pathname, std::string& buf)
{
    try {
        auto const bytes = ::readlink(pathname, buf.data(), buf.size());
        if (bytes == -1) {
            throw error { "readlink()", linux::make_error_code(errno) };
        }
        else if (bytes == static_cast<ssize_t>(buf.size())) {
            std::string buf2;
            buf2.resize(buf.size() * 2);

            readlink(pathname, buf2);

            buf = std::move(buf2);
        }
        else {
            buf[static_cast<std::size_t>(bytes)] = '\0';
            buf.resize(static_cast<std::size_t>(bytes));
        }
    }
    catch (...) {
        rethrow_error({
            { "pathname", pathname },
            { "buffer size", buf.size() },
        });
    }
}

std::string
readlinkat(fd_ref dirfd, cstring_ptr const& pathname)
{
    try {
        std::string result;

        auto const sb = fstatat(dirfd, pathname, AT_SYMLINK_NOFOLLOW);
        if (sb.st_size == 0) {
            result.resize(PATH_MAX);
        }
        else {
            result.resize(static_cast<std::size_t>(sb.st_size) + 1);
        }

        readlinkat(dirfd, pathname, result);

        return result;
    }
    catch (...) {
        rethrow_error({
            { "dirfd", dirfd },
            { "pathname", pathname },
        });
    }
}

void
readlinkat(fd_ref dirfd, cstring_ptr const& pathname, std::string& buf)
{
    try {
        auto const bytes = ::readlinkat(dirfd, pathname, buf.data(), buf.size());
        if (bytes == -1) {
            throw error { "readlinkat()", linux::make_error_code(errno) };
        }
        else if (bytes == static_cast<ssize_t>(buf.size())) {
            std::string buf2;
            buf2.resize(buf.size() * 2);

            readlinkat(dirfd, pathname, buf2);

            buf = std::move(buf2);
        }
        else {
            buf[static_cast<std::size_t>(bytes)] = '\0';
            buf.resize(static_cast<std::size_t>(bytes));
        }
    }
    catch (...) {
        rethrow_error({
            { "dirfd", dirfd },
            { "pathname", pathname },
            { "buffer size", buf.size() },
        });
    }
}

} // namespace stream9::linux
