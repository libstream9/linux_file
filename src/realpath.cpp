#include <stream9/linux/realpath.hpp>

#include <memory>

#include <stdlib.h>

#include <stream9/linux/error.hpp>

namespace stream9::linux {

string
realpath(cstring_ptr const& path)
{
    try {
        struct free_fn {
            void operator()(char* p) const noexcept
            {
                ::free(p);
            }
        };

        std::unique_ptr<char, free_fn> rv {
            ::realpath(path, nullptr)
        };
        if (rv == nullptr) {
            throw error {
                lx::make_error_code(errno), {
                    { "path", path }
                }
            };
        }

        return rv.get();
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::linux
