#include <stream9/linux/file/dup.hpp>

#include <unistd.h>

#include <stream9/bits.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9::linux {

static std::string
flags_to_symbol(int flags)
{
    return bits::ored_flags_to_symbol(flags, {
        STREAM9_FLAG_ENTRY(O_CLOEXEC),
    });
}

fd_ref
dup(fd_ref const oldfd)
{
    try {
        using result_t = safe_integer<int, -1>;

        result_t const rc = ::dup(oldfd);
        if (rc == -1) {
            throw error {
                "dup()",
                linux::make_error_code(errno),
            };
        }

        return rc.value();
    }
    catch (...) {
        rethrow_error({
            { "oldfd", static_cast<int>(oldfd) },
        });
    }
}

fd_ref
dup(fd_ref const oldfd, fd_ref const newfd)
{
    try {
        using result_t = safe_integer<int, -1>;

        result_t const rc = ::dup2(oldfd, newfd);
        if (rc == -1) {
            throw error {
                "dup()",
                linux::make_error_code(errno),
            };
        }

        return rc.value();
    }
    catch (...) {
        rethrow_error({
            { "oldfd", static_cast<int>(oldfd) },
            { "newfd", static_cast<int>(newfd) },
        });
    }
}

fd_ref
dup(fd_ref const oldfd, fd_ref const newfd, int const flags)
{
    try {
        using result_t = safe_integer<int, -1>;

        result_t const rc = ::dup3(oldfd, newfd, flags);
        if (rc == -1) {
            throw error {
                "dup()",
                linux::make_error_code(errno),
            };
        }

        return rc.value();
    }
    catch (...) {
        rethrow_error({
            { "oldfd", static_cast<int>(oldfd) },
            { "newfd", static_cast<int>(newfd) },
            { "flags", flags_to_symbol(flags) },
        });
    }
}

} // namespace stream9::linux
