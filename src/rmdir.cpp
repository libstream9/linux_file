#include <stream9/linux/rmdir.hpp>

#include <fcntl.h>
#include <unistd.h>

namespace stream9::linux {

void
rmdir(cstring_ptr const& p)
{
    auto rc = ::rmdir(p);
    if (rc == -1) {
        throw error {
            "rmdir(2)",
            lx::make_error_code(errno), {
                { "pathname", p }
            }
        };
    }
}

void
rmdir(fd_ref dir, cstring_ptr const& p)
{
    auto rc = ::unlinkat(dir, p, AT_REMOVEDIR);
    if (rc == -1) {
        throw error {
            "rmdir(2)",
            lx::make_error_code(errno), {
                { "dir", dir },
                { "pathname", p }
            }
        };
    }
}

} // namespace stream9::linux
