#include <stream9/linux/file/stat.hpp>

#include <stream9/linux/file/symbol.hpp>

#include <sys/stat.h>

#include <stream9/json.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/to_string.hpp>

namespace stream9::linux {

struct ::stat
stat(cstring_ptr const& pathname)
{
    struct ::stat result;

    if (::stat(pathname, &result) == -1) {
        throw error {
            "stat()",
            linux::make_error_code(errno), {
                { "pathname", pathname },
            }
        };
    }

    return result;
}

struct ::stat
fstat(fd_ref const fd)
{
    struct ::stat result;

    if (::fstat(fd, &result) == -1) {
        throw error {
            "fstat()",
            linux::make_error_code(errno), {
                { "fd", fd },
            }
        };
    }

    return result;
}

struct ::stat
lstat(cstring_ptr const& pathname)
{
    struct ::stat result;

    if (::lstat(pathname, &result) == -1) {
        throw error { "lstat()",
            linux::make_error_code(errno), {
                { "pathname", pathname },
            }
        };
    }

    return result;
}

struct ::stat
fstatat(fd_ref const dirfd,
        cstring_ptr const& pathname,
        int const flags)
{
    struct ::stat result;

    if (::fstatat(dirfd, pathname, &result, flags) == -1) {
        throw error { "fstatat()",
            make_error_code(errno), {
                { "dirfd", dirfd },
                { "pathname", pathname },
                { "flags", fstatat_flags_to_symbol(flags) },
            }
        };
    }

    return result;
}

namespace nothrow {

outcome<struct ::stat, linux::errc>
stat(cstring_ptr const& pathname) noexcept
{
    struct ::stat st;

    if (::stat(pathname, &st) == -1) {
        return { st9::error_tag(), static_cast<lx::errc>(errno) };
    }
    else {
        return st;
    }
}

outcome<struct ::stat, linux::errc>
fstat(fd_ref const fd) noexcept
{
    struct ::stat st;

    if (::fstat(fd, &st) == -1) {
        return { st9::error_tag(), static_cast<lx::errc>(errno) };
    }
    else {
        return st;
    }
}

outcome<struct ::stat, linux::errc>
lstat(cstring_ptr const& pathname) noexcept
{
    struct ::stat st;

    if (::lstat(pathname, &st) == -1) {
        return { st9::error_tag(), static_cast<lx::errc>(errno) };
    }
    else {
        return st;
    }
}

outcome<struct ::stat, linux::errc>
fstatat(fd_ref dirfd,
        cstring_ptr const& pathname,
        int flags) noexcept
{
    struct ::stat st;

    if (::fstatat(dirfd, pathname, &st, flags) == -1) {
        return { st9::error_tag(), static_cast<lx::errc>(errno) };
    }
    else {
        return st;
    }
}


} // namespace nothrow

} // namespace stream9::linux

namespace stream9::json {

void
tag_invoke(value_from_tag, json::value& v, struct ::stat const& st)
{
    using stream9::linux::file_type_and_mode_to_symbol;

    auto& obj = v.emplace_object();
    obj["st_dev"] = st.st_dev;
    obj["st_ino"] = st.st_ino;
    obj["st_mode"] = file_type_and_mode_to_symbol(st.st_mode);
    obj["st_nlink"] = st.st_nlink;
    obj["st_uid"] = st.st_uid;
    obj["st_gid"] = st.st_gid;
    obj["st_rdev"] = st.st_rdev;
    obj["st_size"] = st.st_size;
    obj["st_blksize"] = st.st_blksize;
    obj["st_blocks"] = st.st_blocks;
    obj["st_atim"] = str::to_string(st.st_atim);
    obj["st_mtim"] = str::to_string(st.st_mtim);
    obj["st_ctim"] = str::to_string(st.st_ctim);
}

} // namespace stream9::json
