#include <stream9/linux/mkstemp.hpp>

#include <stdlib.h>

namespace stream9::linux {

std::pair<fd, string>
mkstemp(opt<string_view> prefix)
{
    try {
        string p;

        if (prefix) {
            p = *prefix;
        }
        p += "XXXXXX";

        auto rc = ::mkstemp(p.data());
        if (rc == -1) {
            throw error {
                "mkstemp(3)", lx::make_error_code(errno)
            };
        }

        return { fd(rc), p };
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::linux
