#include <stream9/linux/write_n.hpp>

#include <unistd.h>

namespace stream9::linux {

void
write_n(fd_ref fd,
        void const* buf, safe_integer<int64_t, 0> n_bytes)
{
    auto n_to_write = n_bytes;
    safe_integer<int64_t, 0> n_written = 0;

    try {
        auto ptr = static_cast<char const*>(buf);

        while (n_to_write > 0) {
            auto n = ::write(fd, ptr, n_to_write);
            if (n > 0) {
                n_written += n;
                ptr += n;
                n_to_write -= n;
            }
            else if (n == -1) {
                if (errno == EINTR) continue;
                else {
                    throw error {
                        "write(2)", lx::make_error_code(errno)
                    };
                }
            }
            else { // n == 0, this won't happen on proper POSIX.
                throw error {
                    "write(2)", stream9::errc::internal_error, {
                        { "description", "write(2) return with 0" },
                    }
                };
            }
        }
    }
    catch (...) {
        rethrow_error({
            { "fd", fd },
            { "n_bytes", n_bytes },
            { "n_written", n_written },
        });
    }
}

} // namespace stream9::linux

namespace stream9::linux::nothrow {

write_n_outcome
write_n(fd_ref fd,
        void const* buf,
        safe_integer<int64_t, 0> n_bytes) noexcept
{
    safe_integer<int64_t, 0> n_written = 0;
    auto ptr = static_cast<char const*>(buf);

    while (n_bytes > 0) {
        auto n = ::write(fd, ptr, n_bytes);
        if (n > 0) {
            n_written += n;
            ptr += n;
            n_bytes -= n;
        }
        else if (n == -1) {
            if (errno == EINTR) continue;
            else {
                return { n_written, static_cast<errc>(errno) };
            }
        }
        else { // n == 0
            break; // won't happen on proper POSIX.
        }
    }

    return { n_written, errc() };
}

} // namespace stream9::linux::nothrow
