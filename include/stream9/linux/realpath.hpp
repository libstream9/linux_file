#ifndef STREAM9_LINUX_REALPATH_HPP
#define STREAM9_LINUX_REALPATH_HPP

#include <stream9/string.hpp>
#include <stream9/cstring_ptr.hpp>

#include <stream9/linux/error.hpp>

namespace stream9::linux {

string realpath(cstring_ptr const& path);

} // namespace stream9::linux

#endif // STREAM9_LINUX_REALPATH_HPP
