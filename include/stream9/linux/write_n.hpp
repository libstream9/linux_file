#ifndef STREAM9_LINUX_FILE_WRITE_N_HPP
#define STREAM9_LINUX_FILE_WRITE_N_HPP

#include <stream9/array_view.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/safe_integer.hpp>
#include <stream9/string_view.hpp>

namespace stream9::linux {

void
write_n(fd_ref fd, void const* buf, safe_integer<int64_t, 0> n_bytes);

template<size_t N>
void
write_n(fd_ref fd, char (&v)[N])
{
    write_n(fd, v, N);
}

template<typename T>
void
write_n(fd_ref fd, T&& v)
    requires (!std::is_convertible_v<T, string_view>)
          && std::is_convertible_v<T, array_view<char>>
{
    array_view<char> av { v };
    write_n(fd, av.data(), av.size());
}

inline void
write_n(fd_ref fd, string_view v)
{
    write_n(fd, v.data(), v.size());
}

} // namespace stream9::linux

namespace stream9::linux::nothrow {

struct write_n_outcome
{
    using size_type = safe_integer<int64_t, 0>;

    size_type n_written;
    errc error = {};

    size_type operator*() const noexcept { return n_written; }
    operator bool () const noexcept { return error == errc(); }

    bool operator==(size_type n) const noexcept { return n == n_written; }
    bool operator==(errc e) const noexcept { return error == e; }
};

write_n_outcome
write_n(fd_ref fd,
        void const* buf, safe_integer<int64_t, 0> n_bytes) noexcept;

template<size_t N>
write_n_outcome
write_n(fd_ref fd, char (&v)[N])
{
    return nothrow::write_n(fd, v, N);
}

template<typename T>
write_n_outcome
write_n(fd_ref fd, T&& v)
    requires (!std::is_convertible_v<T, string_view>)
          && std::is_convertible_v<T, array_view<char>>
{
    array_view<char> av { v };
    return nothrow::write_n(fd, av.data(), av.size());
}

inline write_n_outcome
write_n(fd_ref fd, string_view v)
{
    return nothrow::write_n(fd, v.data(), v.size());
}

} // namespace stream9::linux::nothrow

#endif // STREAM9_LINUX_FILE_WRITE_N_HPP
