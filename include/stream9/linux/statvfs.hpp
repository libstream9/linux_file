#ifndef STREAM9_LINUX_FILE_STATVFS_HPP
#define STREAM9_LINUX_FILE_STATVFS_HPP

#include <cstdint>

#include <sys/statvfs.h>

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9::linux {

struct ::statvfs
statvfs(cstring_ptr const& path);

struct ::statvfs
fstatvfs(fd_ref);


/*
 * utility functions
 */
inline safe_integer<std::uint64_t>
total_bytes(struct ::statvfs const& st)
{
    try {
        return st.f_blocks * st.f_frsize;
    }
    catch (...) {
        rethrow_error();
    }
}

inline safe_integer<std::uint64_t>
available_bytes(struct ::statvfs const& st)
{
    try {
        return st.f_bavail * st.f_frsize;
    }
    catch (...) {
        rethrow_error();
    }
}

inline safe_integer<std::uint64_t>
used_bytes(struct ::statvfs const& st)
{
    try {
        return (st.f_blocks - st.f_bfree) * st.f_frsize;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::linux

namespace stream9::json {

void
tag_invoke(json::value_from_tag, json::value&, struct ::statvfs const&);

} // namespace stream9::json

#endif // STREAM9_LINUX_FILE_STATVFS_HPP
