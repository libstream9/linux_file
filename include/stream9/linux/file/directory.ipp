#ifndef STREAM9_LINUX_FILE_DIRECTORY_IPP
#define STREAM9_LINUX_FILE_DIRECTORY_IPP

namespace stream9::linux {

class directory_iterator : public stream9::iterators::iterator_facade<
                                    directory_iterator,
                                    std::input_iterator_tag,
                                    struct ::dirent const& >
{
public:
    directory_iterator() = default;

    directory_iterator(directory& d)
        : m_dir { &d }
    {
        try {
            increment();
        }
        catch (...) {
            rethrow_error();
        }
    }

private:
    friend class stream9::iterators::iterator_core_access;

    struct ::dirent const&
    dereference() const noexcept
    {
        assert(m_cur);
        return *m_cur;
    }

    void
    increment()
    {
        try {
            assert(m_dir);
            auto const o_ent = m_dir->read();
            if (o_ent) {
                m_cur = &*o_ent;
            }
            else {
                m_cur = nullptr;
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

    bool
    equal(directory_iterator const& other) const noexcept
    {
        assert(m_dir == other.m_dir || !m_dir || !other.m_dir);
        return m_cur == other.m_cur; // m_cur is nullptr if it is at end of entries
    }

private:
    directory* m_dir = nullptr;
    struct ::dirent const* m_cur = nullptr;
};

inline directory::
directory(DIR* d) noexcept
    : m_dir { d }
{}

inline outcome<directory, lx::errc> directory::
open(cstring_ptr const& path) noexcept
{
    DIR* d = ::opendir(path);
    if (d) {
        return directory(d);
    }
    else {
        return static_cast<lx::errc>(errno);
    }
}

inline directory::const_iterator directory::
begin()
{
    return *this;
}

inline directory::const_iterator directory::
end() noexcept
{
    return {};
}

inline void directory::
seek(long const loc) noexcept
{
    ::seekdir(m_dir, loc);
}

inline void directory::
rewind() noexcept
{
    ::rewinddir(m_dir);
}

inline std::string directory::
path() const
{
    try {
        return fd().path();
    }
    catch (...) {
        rethrow_error();
    }
}

inline bool
is_block_device(struct ::dirent const& e, fd_ref d)
{
    try {
        return type(e, d) == DT_BLK;
    }
    catch (...) {
        rethrow_error();
    }
}

inline bool
is_character_device(struct ::dirent const& e, fd_ref d)
{
    try {
        return type(e, d) == DT_CHR;
    }
    catch (...) {
        rethrow_error();
    }
}

inline bool
is_directory(struct ::dirent const& e, fd_ref d)
{
    try {
        return type(e, d) == DT_DIR;
    }
    catch (...) {
        rethrow_error();
    }
}

inline bool
is_fifo(struct ::dirent const& e, fd_ref d)
{
    try {
        return type(e, d) == DT_FIFO;
    }
    catch (...) {
        rethrow_error();
    }
}

inline bool
is_symlink(struct ::dirent const& e, fd_ref d)
{
    try {
        return ltype(e, d) == DT_LNK;
    }
    catch (...) {
        rethrow_error();
    }
}

inline bool
is_regular_file(struct ::dirent const& e, fd_ref d)
{
    try {
        return type(e, d) == DT_REG;
    }
    catch (...) {
        rethrow_error();
    }
}

inline bool
is_socket(struct ::dirent const& e, fd_ref d)
{
    try {
        return type(e, d) == DT_SOCK;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::linux

#if __has_include(<stream9/json.hpp>)

#include <concepts>

#include <stream9/json.hpp>

namespace stream9::json {

template<typename T>
    requires std::same_as<std::remove_cvref_t<T>, struct ::dirent>
void
tag_invoke(value_from_tag, value& v, T&& ent)
{
    auto& obj = v.emplace_object();
    obj["d_ino"] = ent.d_ino;
    obj["d_off"] = ent.d_off;
    obj["d_reclen"] = ent.d_reclen;
    obj["d_type"] = stream9::linux::dirent_type_to_symbol(ent.d_type);
    obj["d_name"] = ent.d_name;
}

} // namespace stream9::json

#endif // has__include(<stream9/json.hpp>)

#endif // STREAM9_LINUX_FILE_DIRECTORY_IPP
