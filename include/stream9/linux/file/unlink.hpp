#ifndef STREAM9_LINUX_FILE_UNLINK_HPP
#define STREAM9_LINUX_FILE_UNLINK_HPP

#include <stream9/cstring_ptr.hpp>
#include <stream9/outcome.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/fd.hpp>

namespace stream9::linux {

/*
 * unlink(2)
 */
void
unlink(cstring_ptr const& pathname);

void
unlink(fd_ref dir, cstring_ptr const& pathname);

namespace nothrow {

    outcome<void, errc>
    unlink(cstring_ptr const& pathname);

    outcome<void, errc>
    unlink(fd_ref dir, cstring_ptr const& pathname);

} // namespace nothrow

} // namespace stream9::linux

#endif // STREAM9_LINUX_FILE_UNLINK_HPP
