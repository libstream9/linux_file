#ifndef STREAM9_LINUX_FILE_ACCESS_HPP
#define STREAM9_LINUX_FILE_ACCESS_HPP

#include <stream9/linux/error.hpp>
#include <stream9/linux/namespace.hpp>
#include <stream9/cstring_ptr.hpp>

#include <unistd.h>

namespace stream9::linux {

bool access(cstring_ptr pathname, int mode);

bool euidaccess(cstring_ptr pathname, int mode);

} // namespace stream9::linux

#endif // STREAM9_LINUX_FILE_ACCESS_HPP
