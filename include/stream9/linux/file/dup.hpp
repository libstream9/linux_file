#ifndef STREAM9_LINUX_FILE_DUP_HPP
#define STREAM9_LINUX_FILE_DUP_HPP

#include <fcntl.h> // flag constants

#include <stream9/linux/error.hpp>
#include <stream9/linux/fd.hpp>

namespace stream9::linux {

fd_ref dup(fd_ref oldfd);

fd_ref dup(fd_ref oldfd, fd_ref newfd);

fd_ref dup(fd_ref oldfd, fd_ref newfd, int flags);

} // namespace stream9::linux

#endif // STREAM9_LINUX_FILE_DUP_HPP
