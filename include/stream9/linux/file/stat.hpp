#ifndef STREAM9_LINUX_FILE_STAT_HPP
#define STREAM9_LINUX_FILE_STAT_HPP

#include <fcntl.h>
#include <sys/stat.h>

#include <stream9/linux/error.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/linux/namespace.hpp>
#include <stream9/outcome.hpp>
#include <stream9/cstring_ptr.hpp>

namespace stream9::linux {

/*
 * primitive
 */
struct ::stat
stat(cstring_ptr const& pathname);

struct ::stat
fstat(fd_ref fd);

struct ::stat
lstat(cstring_ptr const& pathname);

struct ::stat
fstatat(fd_ref dirfd, cstring_ptr const& pathname, int flags = 0);

/*
 * convenient overload
 */
inline struct ::stat
stat(fd_ref fd)
{
    return fstat(fd);
}

inline struct ::stat
stat(fd_ref dirfd, cstring_ptr const& pathname)
{
    return fstatat(dirfd, pathname, 0);
}

inline struct ::stat
lstat(fd_ref dirfd, cstring_ptr const& pathname)
{
    return fstatat(dirfd, pathname, AT_SYMLINK_NOFOLLOW);
}

/*
 * predicate
 */
bool is_regular_file(struct ::stat const&) noexcept;
bool is_directory(struct ::stat const&) noexcept;
bool is_character_device(struct ::stat const&) noexcept;
bool is_block_device(struct ::stat const&) noexcept;
bool is_fifo(struct ::stat const&) noexcept;
bool is_symlink(struct ::stat const&) noexcept;
bool is_socket(struct ::stat const&) noexcept;
bool has_set_uid_bit(struct ::stat const&) noexcept;
bool has_set_gid_bit(struct ::stat const&) noexcept;
bool has_sticky_bit(struct ::stat const&) noexcept;

namespace nothrow {

    /*
     * primitive
     */
    outcome<struct ::stat, linux::errc>
    stat(cstring_ptr const& pathname) noexcept;

    outcome<struct ::stat, linux::errc>
    fstat(fd_ref fd) noexcept;

    outcome<struct ::stat, linux::errc>
    lstat(cstring_ptr const& pathname) noexcept;

    outcome<struct ::stat, linux::errc>
    fstatat(fd_ref dirfd, cstring_ptr const& pathname, int flags = 0) noexcept;

    /*
     * convenient overload
     */
    inline outcome<struct ::stat, linux::errc>
    stat(fd_ref fd) noexcept
    {
        return nothrow::fstat(fd);
    }

    inline outcome<struct ::stat, linux::errc>
    stat(fd_ref dirfd, cstring_ptr const& pathname) noexcept
    {
        return nothrow::fstatat(dirfd, pathname, 0);
    }

    inline outcome<struct ::stat, linux::errc>
    lstat(fd_ref dirfd, cstring_ptr const& pathname) noexcept
    {
        return nothrow::fstatat(dirfd, pathname, AT_SYMLINK_NOFOLLOW);
    }

} // namespace nothrow

inline bool
is_regular_file(struct ::stat const& st) noexcept
{
    return S_ISREG(st.st_mode);
}

inline bool
is_directory(struct ::stat const& st) noexcept
{
    return S_ISDIR(st.st_mode);
}

inline bool
is_character_device(struct ::stat const& st) noexcept
{
    return S_ISCHR(st.st_mode);
}

inline bool
is_block_device(struct ::stat const& st) noexcept
{
    return S_ISBLK(st.st_mode);
}

inline bool
is_fifo(struct ::stat const& st) noexcept
{
    return S_ISFIFO(st.st_mode);
}

inline bool
is_link(struct ::stat const& st) noexcept
{
    return S_ISLNK(st.st_mode);
}

inline bool
is_socket(struct ::stat const& st) noexcept
{
    return S_ISSOCK(st.st_mode);
}

inline bool
has_set_uid_bit(struct ::stat const& st) noexcept
{
    return st.st_mode & S_ISUID;
}

inline bool
has_set_gid_bit(struct ::stat const& st) noexcept
{
    return st.st_mode & S_ISGID;
}

inline bool
has_sticky_bit(struct ::stat const& st) noexcept
{
    return st.st_mode & S_ISVTX;
}

} // namespace stream9::linux

namespace stream9::json {

struct value_from_tag;
class value;

void tag_invoke(value_from_tag, json::value&, struct ::stat const&);

} // namespace stream9::json

#endif // STREAM9_LINUX_FILE_STAT_HPP
