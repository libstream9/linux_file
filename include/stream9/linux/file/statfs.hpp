#ifndef STREAM9_LINUX_FILE_STATFS_HPP
#define STREAM9_LINUX_FILE_STATFS_HPP

#include <sys/statfs.h>
#include <sys/statvfs.h>

#include <stream9/cstring_ptr.hpp>
#include <stream9/json.hpp>
#include <stream9/linux/error.hpp>

namespace stream9::linux {

struct ::statfs
statfs(cstring_ptr const& path);

} // namespace stream9::linux

namespace stream9::json {

void
tag_invoke(json::value_from_tag, json::value&, struct ::statfs const&);

} // namespace stream9::json

#endif // STREAM9_LINUX_FILE_STATFS_HPP
