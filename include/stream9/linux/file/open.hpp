#ifndef STREAM9_LINUX_FILE_OPEN_HPP
#define STREAM9_LINUX_FILE_OPEN_HPP

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/linux/namespace.hpp>
#include <stream9/outcome.hpp>

#include <fcntl.h>

namespace stream9::linux {

/* mode = 0666 on default. */

fd open(cstring_ptr const& pathname, int flags);

fd open(cstring_ptr const& pathname, int flags, ::mode_t mode);

fd openat(fd_ref dirfd, cstring_ptr const& pathname, int flags);

fd openat(fd_ref dirfd, cstring_ptr const& pathname, int flags, ::mode_t mode);

namespace nothrow {

    outcome<fd, linux::errc>
    open(cstring_ptr const& pathname, int flags);

    outcome<fd, linux::errc>
    open(cstring_ptr const& pathname, int flags, ::mode_t mode);

    outcome<fd, linux::errc>
    openat(fd_ref dirfd, cstring_ptr const& pathname, int flags);

    outcome<fd, linux::errc>
    openat(fd_ref dirfd, cstring_ptr const& pathname, int flags, ::mode_t mode);

} // namespace nothrow

} // namespace stream9::linux

#endif // STREAM9_LINUX_FILE_OPEN_HPP
