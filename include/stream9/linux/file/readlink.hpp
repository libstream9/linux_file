#ifndef STREAM9_LINUX_FILE_READLINK_HPP
#define STREAM9_LINUX_FILE_READLINK_HPP

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/linux/namespace.hpp>

#include <string>

namespace stream9::linux {

/*
 * readlink(2)
 */
std::string
readlink(cstring_ptr const& pathname);

void
readlink(cstring_ptr const& pathname, std::string& buf);

/*
 * readlinkat(2)
 */
std::string
readlinkat(fd_ref dirfd, cstring_ptr const& pathname);

void
readlinkat(fd_ref dirfd, cstring_ptr const& pathname, std::string& buf);

} // namespace stream9::linux

#endif // STREAM9_LINUX_FILE_READLINK_HPP
