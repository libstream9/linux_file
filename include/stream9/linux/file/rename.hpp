#ifndef STREAM9_LINUX_FILE_RENAME_HPP
#define STREAM9_LINUX_FILE_RENAME_HPP

#include <stdio.h>

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/error.hpp>

namespace stream9::linux {

void
rename(cstring_ptr oldpath, cstring_ptr newpath);

} // namespace stream9::linux

#endif // STREAM9_LINUX_FILE_RENAME_HPP
