#ifndef STREAM9_LINUX_FILE_DIRECTORY_HPP
#define STREAM9_LINUX_FILE_DIRECTORY_HPP

#include "symbol.hpp"

#include <sys/types.h>
#include <dirent.h>

#include <stream9/cstring_ptr.hpp>
#include <stream9/cstring_view.hpp>
#include <stream9/iterators.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/linux/namespace.hpp>
#include <stream9/outcome.hpp>

namespace stream9::linux {

class directory_iterator;

/*
 * @model std::input_range
 * @model !std::copyable
 * @model std::movable
 */
class directory
{
public:
    using const_iterator = directory_iterator; // input_iterator of directory_entry

public:
    // essentials
    directory(cstring_ptr const& pathname);

    directory(linux::fd&&);

    directory(fd_ref dirfd, cstring_ptr const& name);

    static outcome<directory, lx::errc>
    open(cstring_ptr const& path) noexcept;

    ~directory() noexcept;

    directory(directory const&) = delete;
    directory& operator=(directory const&) = delete;

    directory(directory&&) noexcept;
    directory& operator=(directory&&) noexcept;

    // accessor
    const_iterator begin();
    const_iterator end() noexcept;

    // query
    fd_ref fd() const;

    std::string path() const;

    opt<struct dirent const&> read() const;

    long tell() const;

    // modifier
    void seek(long loc) noexcept;

    void rewind() noexcept;

private:
    directory(DIR*) noexcept;

private:
    DIR* m_dir;
};

inline cstring_view
name(struct ::dirent const& e) noexcept
{
    return e.d_name;
}

// Return file type based on dirent.d_type with symlink followed.
// If dirent.d_type is DT_UNKNOWN, then retrieve value with fstatat.
unsigned char type(struct ::dirent const&, fd_ref dirfd);

// Return file type based on dirent.d_type without symlink followed.
// If dirent.d_type is DT_UNKNOWN, then retrieve value with fstatat.
unsigned char ltype(struct ::dirent const&, fd_ref dirfd);

// convenient functions
bool is_block_device(struct ::dirent const&, fd_ref dirfd);
bool is_character_device(struct ::dirent const&, fd_ref dirfd);
bool is_directory(struct ::dirent const&, fd_ref dirfd);
bool is_fifo(struct ::dirent const&, fd_ref dirfd);
bool is_symlink(struct ::dirent const&, fd_ref dirfd);
bool is_regular_file(struct ::dirent const&, fd_ref dirfd);
bool is_socket(struct ::dirent const&, fd_ref dirfd);

inline ::off_t
position(struct ::dirent const& e) noexcept
{
    return e.d_off;
}


} // namespace stream9::linux

#include "directory.ipp"

#endif // STREAM9_LINUX_FILE_DIRECTORY_HPP
