#ifndef STREAM9_LINUX_FILE_READ_ALL_HPP
#define STREAM9_LINUX_FILE_READ_ALL_HPP

#include <functional>
#include <string>

#include <stream9/linux/error.hpp>
#include <stream9/linux/namespace.hpp>
#include <stream9/linux/read.hpp>

namespace stream9::linux {

template<typename Fn>
void
read_all(fd_ref fd, Fn&& append)
    requires std::invocable<Fn, char*, ssize_t>
{
    try {
        char buf[4096];

        while (true) {
            auto o_n = read(fd, buf);
            if (o_n) {
                auto n = *o_n;
                if (n == 0) break;
                std::invoke(append, buf, n);
            }
            else if (o_n == errc::eintr) continue;
            else if (o_n == errc::eagain) break;
            else {
                throw error { make_error_code(o_n.error()) };
            }
        }
    }
    catch (...) {
        rethrow_error({
            { "path", fd.path() }
        });
    }
}

inline void
read_all(fd_ref fd, std::string& s)
{
    try {
        auto append = [&](auto p, auto n) { s.append(p, static_cast<size_t>(n)); };
        read_all(fd, append);
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_FILE_READ_ALL_HPP
