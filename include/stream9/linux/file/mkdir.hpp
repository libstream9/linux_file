#ifndef STREAM9_LINUX_FILE_MKDIR_HPP
#define STREAM9_LINUX_FILE_MKDIR_HPP

#include <sys/stat.h> // mode_t

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/outcome.hpp>

namespace stream9::linux {

void
mkdir(cstring_ptr const& pathname, ::mode_t mode = 0777);

namespace nothrow {

    outcome<void, errc>
    mkdir(cstring_ptr const& pathname, ::mode_t mode = 0777);

} // namespace nothrow

} // namespace stream9::linux

#endif // STREAM9_LINUX_FILE_MKDIR_HPP
