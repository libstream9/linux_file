#ifndef STREAM9_LINUX_FILE_SYMBOL_HPP
#define STREAM9_LINUX_FILE_SYMBOL_HPP

#include <string>

#include <fcntl.h>

namespace stream9::linux {

/*
 * return symbol of file descriptor flags
 *
 * ex) FD_CLOEXEC
 */
std::string
file_descriptor_flags_to_symbol(int flags);

/*
 * return symbol of file status flags
 *
 * ex) O_RDONLY, O_CREATE, O_NONBLOCK, etc...
 */
std::string
file_status_flags_to_symbol(int flags);

/*
 * return symbol of file permission mode flags
 *
 * ex) S_IRUSR, S_IWGRP etc...
 */
std::string
file_mode_to_symbol(::mode_t flags);

std::string
file_type_and_mode_to_symbol(::mode_t);

/*
 * return symbol of flags on fstatat
 *
 * ex) AT_EMPTY_PATH, AT_NO_AUTOMOUNT etc...
 */
std::string
fstatat_flags_to_symbol(int flags);

/*
 * return symbol of prot on mmap
 *
 * ex) PROT_NONE, PROT_READ etc...
 */
std::string
mmap_prot_to_symbol(int prot);

/*
 * return symbol of flags on mmap
 *
 * ex) MAP_PRIVATE, MAP_FIXED etc...
 */
std::string
mmap_flags_to_symbol(int flags);

/*
 * return symbol of dt_type on struct dirent
 *
 * ex) DT_REG, DT_DIR etc...
 */
std::string
dirent_type_to_symbol(unsigned char d_type);

} // namespace stream9::linux

#endif // STREAM9_LINUX_FILE_SYMBOL_HPP
