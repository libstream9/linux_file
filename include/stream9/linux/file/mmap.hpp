#ifndef STREAM9_LINUX_FILE_MMAP_HPP
#define STREAM9_LINUX_FILE_MMAP_HPP

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/linux/namespace.hpp>

#include <cstddef>
#include <span>
#include <string_view>

#include <fcntl.h>
#include <sys/mman.h>

namespace stream9::linux {

/*
 * @model std::movable
 * @model std::totally_ordered
 */
class mmap
{
public:
    // essentials
    mmap(void* addr, std::size_t length,
         int prot, int flags, fd_ref fd, ::off_t offset);

    // open file with O_RDONLY and map it to memory
    mmap(cstring_ptr const& pathname);

    mmap(fd_ref fd);

    ~mmap() noexcept;

    mmap(mmap const&) = delete;
    mmap& operator=(mmap const&) = delete;

    mmap(mmap&&) = default;
    mmap& operator=(mmap&&) = default;

    // accessor
    void* data() const noexcept { return m_addr; }

    // query
    std::size_t size() const noexcept { return m_len; }

    // comparison
    bool operator==(mmap const&) const = default;
    std::strong_ordering operator<=>(mmap const&) const = default;

    // conversion
    operator std::string_view () const noexcept
    {
        return { static_cast<char const*>(m_addr), m_len };
    }

    template<typename T>
    operator std::span<T> () const noexcept
    {
        return { static_cast<T*>(m_addr), m_len / sizeof(T) };
    }

public:
    enum class errc {
        try_to_map_empty_file,
    };

    static std::error_category const& error_category();

    friend std::error_code
    make_error_code(errc const e)
    {
        return { static_cast<int>(e), error_category() };
    }

private:
    void* m_addr {};
    std::size_t m_len;
};

} // namespace stream9::linux

namespace std {

template<>
struct is_error_code_enum<stream9::linux::mmap::errc>
    : true_type {};

} // namespace std

#endif // STREAM9_LINUX_FILE_MMAP_HPP
