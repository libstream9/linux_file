#ifndef STREAM9_LINUX_CHMOD_HPP
#define STREAM9_LINUX_CHMOD_HPP

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/error.hpp>

namespace stream9::linux {

void chmod(cstring_ptr pathname, ::mode_t mode);

} // namespace stream9::linux

#endif // STREAM9_LINUX_CHMOD_HPP
