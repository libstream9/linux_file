#ifndef STREAM9_LINUX_FILE_CHDIR_HPP
#define STREAM9_LINUX_FILE_CHDIR_HPP

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/fd.hpp>

namespace stream9::linux {

void
chdir(cstring_ptr const& path);

void
chdir(fd_ref);

} // namespace stream9::linux

#endif // STREAM9_LINUX_FILE_CHDIR_HPP
