#ifndef STREAM9_LINUX_FILE_LINK_HPP
#define STREAM9_LINUX_FILE_LINK_HPP

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/fd.hpp>

#include <fcntl.h>
#include <unistd.h>

namespace stream9::linux {

void
linkat(fd_ref olddirfd, cstring_ptr const& oldpath,
       fd_ref newdirfd, cstring_ptr const& newpath, int flags = 0);

void
linkat(fd_ref oldfilefd,
       fd_ref newdirfd, cstring_ptr const& newpath);

inline void
linkat(cstring_ptr const& oldpath,
       cstring_ptr const& newpath, int flags = 0)
{
    linkat(AT_FDCWD, oldpath, AT_FDCWD, newpath, flags);
}

inline void
linkat(fd_ref oldfilefd, cstring_ptr const& newpath)
{
    linkat(oldfilefd, AT_FDCWD, newpath);
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_FILE_LINK_HPP
