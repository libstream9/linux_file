#ifndef STREAM9_LINUX_SYMLINK_HPP
#define STREAM9_LINUX_SYMLINK_HPP

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/error.hpp>

#include <unistd.h>

namespace stream9::linux {

void symlink(cstring_ptr target, cstring_ptr linkpath);

} // namespace stream9::linux

#endif // STREAM9_LINUX_SYMLINK_HPP
