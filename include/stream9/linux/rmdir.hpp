#ifndef STREAM9_LINUX_FILE_RMDIR_HPP
#define STREAM9_LINUX_FILE_RMDIR_HPP

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/fd.hpp>

namespace stream9::linux {

/*
 * rmdir(2)
 */
void
rmdir(cstring_ptr const& pathname);

void
rmdir(fd_ref dir, cstring_ptr const& pathname);

} // namespace stream9::linux

#endif // STREAM9_LINUX_FILE_RMDIR_HPP
