#ifndef STREAM9_LINUX_REMOVE_HPP
#define STREAM9_LINUX_REMOVE_HPP

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/error.hpp>

namespace stream9::linux {

/*
 * remove(3)
 */
void
remove(cstring_ptr const& pathname);

} // namespace stream9::linux

#endif // STREAM9_LINUX_REMOVE_HPP
