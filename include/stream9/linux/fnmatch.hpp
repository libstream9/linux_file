#ifndef STREAM9_LINUX_FNMATCH_HPP
#define STREAM9_LINUX_FNMATCH_HPP

#include <stream9/cstring_ptr.hpp>

#include <fnmatch.h>

namespace stream9::linux {

bool
fnmatch(cstring_ptr const& pattern,
        cstring_ptr const& pathname,
        int flags);

} // namespace stream9::linux

#endif // STREAM9_LINUX_FNMATCH_HPP
