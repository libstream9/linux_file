#ifndef STREAM9_LINUX_READ_N_HPP
#define STREAM9_LINUX_READ_N_HPP

#include <stream9/array_view.hpp>
#include <stream9/safe_integer.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/fd.hpp>

#include <cstdint>

namespace stream9::linux {

using std::int64_t;

/*
 * Read bytes from fd until
 * - It hit the end of the file.
 * - It fill whole buffer.
 * - An error occur while reading.
 *
 * It will continue reading when
 * - It had read the data but there is still room in the buffer.
 * - It has interupted by a signal.
 */
struct read_n_outcome
{
    safe_integer<int64_t, 0> n;
    errc ec;
    bool is_eof : 1;

    explicit operator bool () const noexcept { return ec == errc(); }

    safe_integer<int64_t, 0> operator*() const noexcept { return n; }

    bool operator==(errc e) const noexcept { return ec == e; }
};

read_n_outcome
read_n(fd_ref fd,
       array_view<char> buf) noexcept;

} // namespace stream9::linux

#endif // STREAM9_LINUX_READ_N_HPP
