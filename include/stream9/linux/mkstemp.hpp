#ifndef STREAM9_LINUX_MKSTEMP_HPP
#define STREAM9_LINUX_MKSTEMP_HPP

#include <stream9/linux/error.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/optional.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

#include <utility>

namespace stream9::linux {

std::pair<fd, string/*path*/>
mkstemp(opt<string_view> prefix = {});

} // namespace stream9::linux

#endif // STREAM9_LINUX_MKSTEMP_HPP
