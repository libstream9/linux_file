#ifndef STREAM9_LINUX_GETCWD_HPP
#define STREAM9_LINUX_GETCWD_HPP

#include <stream9/string.hpp>

#include <stream9/linux/error.hpp>

namespace stream9::linux {

string getcwd();

} // namespace stream9::linux

#endif // STREAM9_LINUX_GETCWD_HPP
