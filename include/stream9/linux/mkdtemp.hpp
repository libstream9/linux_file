#ifndef STREAM9_LINUX_MKDTEMP_HPP
#define STREAM9_LINUX_MKDTEMP_HPP

#include <stream9/linux/error.hpp>
#include <stream9/string.hpp>

namespace stream9::linux {

string
mkdtemp();

string
mkdtemp(string_view path_prefix);

} // namespace stream9::linux

#endif // STREAM9_LINUX_MKDTEMP_HPP
